------------------------------------------------------
Twitter data organizer algorithm for massive data sets
------------------------------------------------------


The algorithm takes as input twitter data in the format and two dates (start and end date) in the first line of the input:

TimeStamp (DD/MM/YYYY), Department/Tweet/Tweet Information, Count

and outputs the organized data from the corresponding start and end dates
