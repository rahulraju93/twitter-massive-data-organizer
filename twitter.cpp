#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    vector<vector<string>> input;
    vector<string> temp;                      //storing the strings in vectors
    vector<vector<string>> input_parsed;
    vector<string> temp_parsed;               //parsing them into tokens of individual values 
    vector<vector<string>> output;
    vector<string> out_temp;                  //output vector to return the right values
    
    string str;
    string str_2;
    string j;   
    string line;

    getline(cin, str);                        //get the first line for input
    temp.push_back(str);
    input.push_back(temp);
    temp.clear();	                      //store in the input vector for parsing
    cout << "\n";                             //blank line in input
     
    while(getline(cin,line) && line != "")
        {
        temp.push_back(line);
        input.push_back(temp);
        temp.clear();
        }                                     //get all lines for input vector 

    for(int i = 0;i < input.size();i++)
        {
                j = accumulate(begin(input[i]),end(input[i]),j);     //parse vector into token vector
                istringstream ss(j);
                while(getline(ss,str_2,','))
                    temp_parsed.push_back(str_2);           
                input_parsed.push_back(temp_parsed);
		temp_parsed.clear();
		j.clear();      
        }

int avoid = input_parsed[0][0].find_first_not_of(" "); //check for substring to compare date
string con = input_parsed[0][0].substr(avoid,7);

int avoid_2 = input_parsed[0][1].find_first_not_of(" "); //check for substring to compare date
string con_2 = input_parsed[0][1].substr(avoid_2,7);
    
	
    for(int i = 1;i < input_parsed.size();i++) //checking for the date in range of input values and pushing in output vector
        {
	    int space_avoid = input_parsed[i][0].find_first_not_of(" ");
            string x = input_parsed[i][0].substr(space_avoid,7);
            if(x.compare(con) >= 0 && x.compare(con_2) <= 0 && stoi(input_parsed[i][2]) > 0)
                {
		    out_temp.push_back(input_parsed[i][0]);
		    out_temp.push_back(",");
		    out_temp.push_back(input_parsed[i][1]);
                    out_temp.push_back(",");
		    out_temp.push_back(input_parsed[i][2]);	
                    for(int j = i + 1; j < input_parsed.size();j++)
                        {
			 int space_avoid_2 = input_parsed[j][0].find_first_not_of(" ");
                         string y = input_parsed[j][0].substr(space_avoid_2,7);   
                         if(y.compare(x) == 0 && stoi(input_parsed[j][2]) > 0)
                             {
                               out_temp.push_back(",");
                               out_temp.push_back(input_parsed[j][1]);
                               out_temp.push_back(",");
                               out_temp.push_back(input_parsed[j][2]);
			       input_parsed[j][0] = "0";
 			       input_parsed[j][1] = "0";
			       input_parsed[j][2] = "0";	 
                             }
                        }
                }
	if(!out_temp.empty())   //make sure to only push vectors that are populated
	{
	output.push_back(out_temp);
	out_temp.clear();
        }
    }

    for(int i = 0;i < output.size();i++)
	{
	int cut = output[i][0].find_first_not_of(" "); //check for substring to compare date
	string lux = output[i][0].substr(cut,7);
	output[i][0] = lux;	
	}	

    for(int i = 0; i < output.size();i++)  //output the vector 
      {
	for(int j = 0;j < output[i].size();j++)
	{
		cout << output[i][j];
	}
	cout << "\n";	       
      }
    
    return 0;
}
